Simple Prometheus prober for Icecast streams.

It will continuously attempt to connect and download a specified
stream, exporting metrics on bandwidth and connection failures.
