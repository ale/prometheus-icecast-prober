package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	remoteURL = flag.String("url", "", "stream URL")
	addr      = flag.String("addr", ":7766", "address for HTTP listener")

	reconnectDelay = 3 * time.Second

	// Exported metrics.
	connections = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "connections",
			Help: "Number of connections",
		},
		[]string{"status"},
	)
	bytesReceived = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "bytes_received",
			Help: "Bytes received",
		},
	)
)

const bufSize = 4096

func init() {
	prometheus.MustRegister(connections)
	prometheus.MustRegister(bytesReceived)
}

type listener struct {
	url string
}

// Receive a stream content over HTTP. We're using the standard Go
// client as it can handle this case, no need to hijack the connection.
func (l *listener) stream() {
	resp, err := http.Get(l.url)
	if err != nil {
		log.Printf("connection error: %v", err)
		connections.With(prometheus.Labels{"status": "error"}).Inc()
		return
	}
	defer resp.Body.Close()

	connections.With(prometheus.Labels{"status": "ok"}).Inc()

	buf := make([]byte, bufSize)
	for {
		n, err := resp.Body.Read(buf)
		if err != nil {
			log.Printf("read error: %v", err)
			return
		}

		bytesReceived.Add(float64(n))
	}
}

func (l *listener) run() {
	for {
		l.stream()

		// Limit the number of outgoing connections by
		// sleeping a bit before retrying.
		time.Sleep(reconnectDelay)
	}
}

func main() {
	flag.Parse()

	if *remoteURL == "" {
		log.Fatal("--url must be set")
	}

	// Spawn the stream listener in a background goroutine. This
	// can't "fail" so we don't need to catch its errors.
	go func() {
		l := &listener{url: *remoteURL}
		l.run()
	}()

	// Start the HTTP listener to export Prometheus metrics.
	http.Handle("/metrics", promhttp.Handler())
	if err := http.ListenAndServe(*addr, nil); err != http.ErrServerClosed {
		log.Fatal(err)
	}
}
